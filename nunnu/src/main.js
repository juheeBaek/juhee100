import Vue from 'vue'
import App from './App.vue'
// import router from '/routes'
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#464E8C',
    secondary: '#65C2C2',
    info:'#9FA0A0',
    accent: '#8c9eff',
    success: '#1976d2',
    error: '#b71c1c',
    warning: '#e65100',
    hw: '#E91E63',
    sw: '#3F51B5',
    service: '#FFC107',
    biz: '#009688',
  },

})

new Vue({
  el: '#app',
  // router,
  render: h => h(App)
})
