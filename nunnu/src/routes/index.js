import Vue from 'vue'
import Router from 'vue-router'

import Introduce from '../views/IntroduceView.vue'
import Member from '../views/MemberView.vue'
import Ranking from '../views/RankingView.vue'
import Report from '../views/ReportView.vue'

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  // 라우터 정보가 들어갈 속성
  // 1. url 경로
  // 2. url 경로에 해당하는 컴포넌트

  routes: [
    {
      path: '/report',
      component: Report,
      meta: {
        title: 'Nunnu-Report'
      }
    },
    {
      path: '/rank',
      component: Ranking,
      meta: {
        title: 'Nunnu-Rank'
      }
    },
    {
      path: '/member',
      component: Member,
      meta: {
        title: 'Nunnu-Member'
      }
    },
    {
      path: '/intro',
      component: Introduce,
      meta: {
        title: 'Nunnu-Intro'
      }
    },
  ]
})
